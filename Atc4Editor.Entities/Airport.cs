﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

namespace Atc4Editor.Entities
{
    /// <summary>A destination or origin airport.</summary>
    public sealed class Airport
    {
        private string _iataCode;
        private string _icaoCode;

        /// <summary>
        ///     The airport ICAO code. This is a unique four character code, which not all airports have, but all major ones
        ///     do.
        /// </summary>
        public string IcaoCode
        {
            get => _icaoCode;
            set => _icaoCode = value.ToUpperInvariant();
        }

        /// <summary>
        ///     The airport IATA code. This is a unique three character code, which not all airports have, but all major ones
        ///     do.
        /// </summary>
        public string IataCode
        {
            get => _iataCode;
            set => _iataCode = value.ToUpperInvariant();
        }
    }
}

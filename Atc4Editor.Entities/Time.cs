﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Atc4Editor.Entities.Resources;

namespace Atc4Editor.Entities
{
    /// <summary>Time of the day as represented in the ATC4 game.</summary>
    public struct Time : IComparable<Time>
    {
        private static readonly Regex SimpleTimeRegex = new Regex(@"\A(\d{3,4})\z");
        private static readonly Regex ComplexTimeRegex = new Regex(@"\A(\d{3,4})\.(\d{2})\z");

        /// <summary>Hour of the day [0-23].</summary>
        public int Hour { get; }

        /// <summary>Minute of the hour [0-59].</summary>
        public int Minute { get; }

        /// <summary>Second of the minute [0-59].</summary>
        public int Second { get; set; }

        /// <summary>Creates a new time.</summary>
        /// <param name="hour">The hour of the day.</param>
        /// <param name="minute">The minute of the hour.</param>
        /// <param name="second">The second of the minute.</param>
        public Time(int hour, int minute, int second = 0)
        {
            if(hour < 0 || hour >= 24)
                throw new ArgumentOutOfRangeException(nameof(hour), hour,
                    string.Format(CultureInfo.CurrentCulture, Strings.HourOutOfRange));
            if(minute < 0 || minute >= 60)
                throw new ArgumentOutOfRangeException(nameof(minute), minute,
                    string.Format(CultureInfo.CurrentCulture, Strings.MinuteOutOfRange));
            if(second < 0 || second >= 60)
                throw new ArgumentOutOfRangeException(nameof(minute), minute,
                    string.Format(CultureInfo.CurrentCulture, Strings.SecondOutOfRange));

            Hour = hour;
            Minute = minute;
            Second = second;
        }

        /// <summary>Creates a new time.</summary>
        /// <param name="time">The time in string format. Must be formatted HHmm.</param>
        public Time(string time)
        {
            if(string.IsNullOrEmpty(time)) throw new ArgumentNullException(nameof(time));

            int timeInt, seconds = 0;

            Match simpleTimeMatch = SimpleTimeRegex.Match(time);
            if(simpleTimeMatch.Success)
            {
                timeInt = int.Parse(simpleTimeMatch.Groups[1].Value);
            }
            else
            {
                Match complexTimeMatch = ComplexTimeRegex.Match(time);
                if(!complexTimeMatch.Success)
                    throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, Strings.WrongTimeFormat, time));
                timeInt = int.Parse(complexTimeMatch.Groups[1].Value);
                seconds = int.Parse(complexTimeMatch.Groups[2].Value);
            }

            int hour = timeInt / 100;
            int minute = timeInt % 100;

            if(hour < 0 || hour >= 24)
                throw new ArgumentOutOfRangeException(nameof(hour), hour,
                    string.Format(CultureInfo.CurrentCulture, Strings.HourOutOfRange));
            if(minute < 0 || minute >= 60)
                throw new ArgumentOutOfRangeException(nameof(minute), minute,
                    string.Format(CultureInfo.CurrentCulture, Strings.MinuteOutOfRange));
            if(seconds < 0 || seconds >= 60)
                throw new ArgumentOutOfRangeException(nameof(minute), minute,
                    string.Format(CultureInfo.CurrentCulture, Strings.SecondOutOfRange));

            Hour = hour;
            Minute = minute;
            Second = seconds;
        }

        /// <summary>Creates a new time based on this one with minutes added to it. Handles rolling over hours and days.</summary>
        /// <param name="minutes">The amount of minutes to add.</param>
        /// <returns>A new time with minutes added.</returns>
        public Time AddMinutes(int minutes)
        {
            if(minutes < 0) return SubtractMinutes(-minutes);
            int newMinutes = (Minute + minutes) % 60;
            int newHours = (Hour + (Minute + minutes) / 60) % 24;
            return new Time(newHours, newMinutes);
        }

        /// <summary>Creates a new time based on this one with minutes subtracted from it. Handles rolling over hours and days.</summary>
        /// <param name="minutes">The amount of minutes to subtract.</param>
        /// <returns>A new time with minutes subtracted.</returns>
        public Time SubtractMinutes(int minutes)
        {
            if(minutes < 0) return AddMinutes(-minutes);
            int newMinutes = ((Minute - minutes) % 60 + 60) % 60;
            int newHours = ((Hour + (int)Math.Floor(((double)Minute - minutes) / 60)) % 24 + 24) % 24;
            return new Time(newHours, newMinutes);
        }

        /// <summary>Turns the instance a readable string, for debugging purposes.</summary>
        /// <returns>A string that can be used by a human to identify the instance.</returns>
        public override string ToString() =>
            Hour.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0')
            + Minute.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0');

        /// <summary>Checks whether two times are equal. Times are considered equal when they represent the same time.</summary>
        /// <param name="other">The time to compare this one to.</param>
        /// <returns>True iff the hours and minutes of the two times are the same.</returns>
        public bool Equals(Time other) => Hour == other.Hour && Minute == other.Minute;

        /// <summary>Checks whether an object is equal to this time. Times are considered equal when they represent the same time.</summary>
        /// <param name="other">The time to compare this one to.</param>
        /// <returns>True iff the other object is a <see cref="Time"/> and the hours and minutes of the two times are the same.</returns>
        public override bool Equals(object obj)
        {
            if(ReferenceEquals(null, obj)) return false;
            return obj is Time time && Equals(time);
        }

        /// <summary>Get a hash code for this time.</summary>
        /// <returns>The hash code for this time.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return (Hour * 397) ^ Minute;
            }
        }

        /// <summary>Compares this time to another time.</summary>
        /// <param name="other">The time to compare to.</param>
        /// <returns>1 if this time is larger than the other, or the other is null. 0 if they are equal. -1 otherwise.</returns>
        public int CompareTo(Time other)
        {
            if(other == null || this > other) return 1;
            if(this == other) return 0;
            return -1;
        }

        /// <summary>Get a new time which is <paramref name="time1"/> - <paramref name="time2"/>.</summary>
        /// <param name="time1">The base time.</param>
        /// <param name="time2">The amount of time to subtract from <paramref name="time1"/>.</param>
        /// <returns>time1 - time2</returns>
        public static Time operator -(Time time1, Time time2) => time1.SubtractMinutes(time2.Hour * 60 + time2.Minute);

        /// <summary>Check whether <paramref name="time1"/> is earlier than <paramref name="time2"/>.</summary>
        /// <param name="time1">The expected earlier time.</param>
        /// <param name="time2">The expected later time.</param>
        /// <returns>Whether time1 is before time2.</returns>
        public static bool operator <(Time time1, Time time2) =>
            time1.Hour < time2.Hour || time1.Hour == time2.Hour && time1.Minute < time2.Minute;

        /// <summary>Check whether <paramref name="time1"/> is later than <paramref name="time2"/>.</summary>
        /// <param name="time1">The expected later time.</param>
        /// <param name="time2">The expected earlier time.</param>
        /// <returns>Whether time1 is after time2.</returns>
        public static bool operator >(Time time1, Time time2) =>
            time1.Hour > time2.Hour || time1.Hour == time2.Hour && time1.Minute > time2.Minute;

        /// <summary>Check whether <paramref name="time1"/> is earlier than or equal to <paramref name="time2"/>.</summary>
        /// <param name="time1">The expected later or equal time.</param>
        /// <param name="time2">The expected earlier or equal time.</param>
        /// <returns>Whether time1 is after or equal to time2.</returns>
        public static bool operator >=(Time time1, Time time2) => time1 > time2 || time1 == time2;

        /// <summary>Check whether <paramref name="time1"/> is earlier than or equal to <paramref name="time2"/>.</summary>
        /// <param name="time1">The expected earlier or equal time.</param>
        /// <param name="time2">The expected later or equal time.</param>
        /// <returns>Whether time1 is before or equal to time2.</returns>
        public static bool operator <=(Time time1, Time time2) => time1 < time2 || time1 == time2;

        /// <summary>Check whether <paramref name="time1"/> is equal to <paramref name="time2"/>.</summary>
        /// <param name="time1">A time.</param>
        /// <param name="time2">Another time.</param>
        /// <returns>Whether the two times are equal.</returns>
        public static bool operator ==(Time time1, Time time2) => time1.Hour == time2.Hour && time1.Minute == time2.Minute;

        /// <summary>Check whether <paramref name="time1"/> is different from <paramref name="time2"/>.</summary>
        /// <param name="time1">A time.</param>
        /// <param name="time2">Another time.</param>
        /// <returns>Whether the two times are different.</returns>
        public static bool operator !=(Time time1, Time time2) => time1.Hour != time2.Hour || time1.Minute != time2.Minute;
    }
}

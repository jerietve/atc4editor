﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Atc4Editor.Entities.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Atc4Editor.Entities.Resources.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Scenario duration must be positive. Received: {0}..
        /// </summary>
        internal static string DurationMustBePositive {
            get {
                return ResourceManager.GetString("DurationMustBePositive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hour must be in range [0, 23]..
        /// </summary>
        internal static string HourOutOfRange {
            get {
                return ResourceManager.GetString("HourOutOfRange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The month must be a value between 1 and 12, both inclusive. Received: {0}..
        /// </summary>
        internal static string IncorrectValueForMonth {
            get {
                return ResourceManager.GetString("IncorrectValueForMonth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Minute must be in range [0, 59]..
        /// </summary>
        internal static string MinuteOutOfRange {
            get {
                return ResourceManager.GetString("MinuteOutOfRange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Second must be in range [0, 59]..
        /// </summary>
        internal static string SecondOutOfRange {
            get {
                return ResourceManager.GetString("SecondOutOfRange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can&apos;t parse string as squawk: &quot;{0}&quot;. A squawk should be four digits from 0 to 7..
        /// </summary>
        internal static string SquawkShouldBeNumbers {
            get {
                return ResourceManager.GetString("SquawkShouldBeNumbers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time must be formatted as three or four digits representing the 24 hour time, optionally followed by a period and the number of seconds. Received: {0}..
        /// </summary>
        internal static string WrongTimeFormat {
            get {
                return ResourceManager.GetString("WrongTimeFormat", resourceCulture);
            }
        }
    }
}

﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

namespace Atc4Editor.Entities
{
    /// <summary>An aircraft model.</summary>
    public sealed class Model
    {
        /// <summary>The size of the aircraft model in the game world.</summary>
        public enum Size
        {
            Small,
            Medium,
            Large
        }

        /// <summary>The purpose for which this aircraft model is used, what it transports.</summary>
        public enum Type
        {
            Passengers,
            Cargo,
            Business,
            CoastGuard,
            Fighter,
            MilitaryPassengers
        }

        private string _aircraftIcaoCode;
        private string _airlineIcaoCode;

        /// <summary>The ICAO code of the aircraft type.</summary>
        public string AircraftIcaoCode
        {
            get => _aircraftIcaoCode;
            set => _aircraftIcaoCode = value.ToUpperInvariant();
        }

        /// <summary>The ICAO code of the airline.</summary>
        public string AirlineIcaoCode
        {
            get => _airlineIcaoCode;
            set => _airlineIcaoCode = value.ToUpperInvariant();
        }

        /// <summary>The name of the folder containing the model files.</summary>
        public string Folder { get; set; }

        /// <summary>Whether this livery is unique (only one should be present at a time in a scenario).</summary>
        public bool UniqueLivery { get; set; }

        /// <summary>The <see cref="Type"/> of aircraft, what it is used for.</summary>
        public Type AircraftType { get; set; }

        /// <summary>The <see cref="Size"/> of the aircraft.</summary>
        public Size AircraftSize { get; set; }
    }
}

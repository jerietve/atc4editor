﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using Atc4Editor.Entities.Routing;

namespace Atc4Editor.Entities
{
    /// <summary>
    ///     A movement describes the departure, arrival, or towing of an aircraft, or a stationary aircraft. The
    ///     properties of this class closely follow the decoded format of a scenario file's SHIP sections. See the wiki for a
    ///     detailed explanation of all properties.
    /// </summary>
    public sealed class Movement
    {
        /// <summary>The (initial) state of the movement.</summary>
        public enum MovementState
        {
            Default,
            SnaPushbackStart,
            SnaPushbackEnd,
            SnaTaxiStart,
            TaxiLoop,
            SnaHoldShort,
            SnaTakeoff,
            SnaTakeoffAtc,
            Takeoff,
            Approach,
            VisualApproachWait,
            SnaFinal
        }

        /// <summary>The type of aircraft movement.</summary>
        public enum MovementType
        {
            Departing,
            Arriving,
            Towing,
            Stationary
        }

        public string ShipCode { get; set; }
        public string CallSign { get; set; }
        public Squawk Squawk { get; set; }
        public MovementType Type { get; set; } // convert to backcolor
        public string Runway { get; set; } // Will be a spot for towing movements
        public string Spot { get; set; }
        public string IcaoCode { get; set; }
        public string DepartureAirport { get; set; }
        public string ArrivalAirport { get; set; }
        public Time Start { get; set; }
        public Time Ready { get; set; }
        public bool AppearsFromStart { get; set; }
        public int? InitialHeading { get; set; }
        public double? InitialDistance { get; set; } // ??
        public int InitialAltitude { get; set; }
        public int InitialSpeed { get; set; }
        public string InitialWaypoint { get; set; }
        public int GroundAlt { get; set; } // ?? rename once you know what it is
        public string GroundNode { get; set; } // ??
        public bool GroundPushback { get; set; }
        public bool GroundHold { get; set; } // ??
        public int GroundHideSpotIn { get; set; } // ?? rename once you know what it is
        public int LinkCount { get; set; } // ??
        public int LinkPosition { get; set; } // ??
        public int LinkDeparture { get; set; } // ??
        public Route Route { get; set; }
        public string Registration { get; set; }
        public Section Section { get; set; }
        public MovementState State { get; set; }
        public int ExtColor { get; set; } // ?? rename once you know what it is
        public int SetupValue { get; set; } // ??
        public int SetupValue2 { get; set; } // ??
        public int SetupValue3 { get; set; } // ??
        public int SetupValue4 { get; set; } // ??
    }
}

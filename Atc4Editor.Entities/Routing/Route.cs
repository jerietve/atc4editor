﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;

namespace Atc4Editor.Entities.Routing
{
    /// <summary>A route is a series of route parts which an aircraft follows in the sky.</summary>
    public sealed class Route
    {
        /// <summary>Creates a new route made up of two parts.</summary>
        /// <param name="part1">The first part of the route.</param>
        /// <param name="part2">The second part of the route.</param>
        public Route(RoutePart part1, RoutePart part2) => RouteParts = new List<RoutePart>(2) {part1, part2};

        /// <summary>Creates a new route made up of three parts.</summary>
        /// <param name="part1">The first part of the route.</param>
        /// <param name="part2">The second part of the route.</param>
        /// <param name="part3">The third part of the route.</param>
        public Route(RoutePart part1, RoutePart part2, RoutePart part3) => RouteParts = new List<RoutePart>(3) {part1, part2, part3};

        /// <summary>Creates a new route made up of four parts.</summary>
        /// <param name="part1">The first part of the route.</param>
        /// <param name="part2">The second part of the route.</param>
        /// <param name="part3">The third part of the route.</param>
        /// <param name="part4">The fourth part of the route.</param>
        public Route(RoutePart part1, RoutePart part2, RoutePart part3, RoutePart part4) =>
            RouteParts = new List<RoutePart>(4) {part1, part2, part3, part4};

        /// <summary>The parts of the route, in order.</summary>
        public IList<RoutePart> RouteParts { get; set; }
    }
}

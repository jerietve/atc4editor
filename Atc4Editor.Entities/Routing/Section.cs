﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

namespace Atc4Editor.Entities.Routing
{
    /// <summary>The type of air traffic control, such as Tower and Ground.</summary>
    public enum Section
    {
        Default = 0,
        Gnd,
        Twr,
        App
    }
}

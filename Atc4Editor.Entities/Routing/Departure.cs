﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;

namespace Atc4Editor.Entities.Routing
{
    /// <summary>
    ///     A departure is a part of a route from a runway leading away from the airport to a transition point. It covers
    ///     the runway transition and common route parts of a Standard Instrument Departure (SID) when it describes a SID.
    /// </summary>
    public sealed class Departure : RoutePart
    {
        /// <summary>Creates a new <see cref="Departure"/>.</summary>
        /// <param name="name">The name of the departure.</param>
        public Departure(string name) => Name = name;

        /// <summary>
        ///     The points making up the departure. Normally the first point is a point on a runway and the last point is the
        ///     first point of a transition.
        /// </summary>
        public List<Point> Points { get; set; }
    }
}

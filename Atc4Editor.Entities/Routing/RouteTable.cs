﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;

namespace Atc4Editor.Entities.Routing
{
    /// <summary>
    ///     A container of routes for departure from or arrival to each active runway for each area under certain
    ///     conditions.
    /// </summary>
    public struct RouteTable
    {
        /// <summary>Whether this is a departure or arrival table.</summary>
        public RouteTableType TableType { get; set; }

        /// <summary>The "comment" field of the route table, a human readable description of what it is for.</summary>
        public string Description { get; set; }

        /// <summary>The time of day from which this table is valid, inclusive.</summary>
        public Time StartTime { get; set; }

        /// <summary>The time of day until which this table is valid, inclusive.</summary>
        public Time EndTime { get; set; }

        /// <summary>The clockwise start of the wind direction range for which this table is valid, inclusive.</summary>
        public int FromWindDirection { get; set; }

        /// <summary>The clockwise end of the wind direction range for which this table is valid, inclusive.</summary>
        public int ToWindDirection { get; set; }

        /// <summary>The minimum visibility in meters for which this table is valid, inclusive.</summary>
        public int FromVisibility { get; set; }

        /// <summary>The maximum visibility in meters for which this table is valid, exclusive.</summary>
        public int ToVisibility { get; set; }

        /// <summary>The runways for which, in this order, routes are provided in <see cref="AreaRoutes"/>.</summary>
        public IList<string> Runways { get; set; }

        /// <summary>The runways which are supposed to be used for this table.</summary>
        public IList<string> ActiveRunways { get; set; }

        /// <summary>The routes for each area. One entry per area with a list of routes in the same order as <see cref="Runways"/>.</summary>
        public IDictionary<string, IList<string>> AreaRoutes { get; set; }

        /// <summary>The type of a <see cref="RouteTable"/>.</summary>
        public enum RouteTableType
        {
            Approach,
            Departure
        }
    }
}

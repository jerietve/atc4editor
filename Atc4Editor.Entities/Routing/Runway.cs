﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Atc4Editor.Entities.Routing
{
    /// <summary>A runway is used for takeoff and landing.</summary>
    public sealed class Runway : RoutePart
    {
        /// <summary>A runway specifier helps distinguish two or three parallel runways from one another.</summary>
        public enum Specifier
        {
            Left,
            Center,
            Right
        }

        private const string RunwaySymbol = "RWY";
        private static readonly Regex RunwayNameRegex = new Regex("^" + RunwaySymbol + @"(\d{1,2})([LRC]?)$");

        /// <summary>Creates a new runway.</summary>
        /// <param name="runwayName">
        ///     The runway name. Must be "RWY" followed by a number between 1 and 36, not starting with zero.
        ///     Optionally ends in L, R, or C.
        /// </param>
        public Runway(string runwayName)
        {
            Match match = RunwayNameRegex.Match(runwayName);
            if(!match.Success) throw new ArgumentException("Invalid runway name: " + runwayName, nameof(runwayName));
            int designation = int.Parse(match.Groups[1].Captures[0].Value);
            if(designation < 1 || designation > 36) throw new ArgumentException("Invalid runway number: " + runwayName);
            Name = runwayName;
        }

        /// <summary>Creates a new runway.</summary>
        /// <param name="designation">A number  between 1 and 36, both inclusive.</param>
        public Runway(int designation)
        {
            if(designation < 1 || designation > 36) throw new ArgumentException(nameof(designation));

            Name = RunwaySymbol + designation.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>Creates a new runway.</summary>
        /// <param name="designation">A number  between 1 and 36, both inclusive.</param>
        /// <param name="specifier">The runway specifier.</param>
        public Runway(int designation, Specifier specifier)
        {
            if(designation < 1 || designation > 36) throw new ArgumentException(nameof(designation));

            Name = RunwaySymbol + designation.ToString(CultureInfo.InvariantCulture) + specifier.ToString().Substring(1);
        }
    }
}

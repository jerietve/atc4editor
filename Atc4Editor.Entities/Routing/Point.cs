﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

namespace Atc4Editor.Entities.Routing
{
    /// <summary>A point in game space, used for describing routes and areas.</summary>
    public sealed class Point
    {
        /// <summary>The distance in meters to the east of the airport coordinates.</summary>
        public double East { get; set; }

        /// <summary>The distance in meters up from sea level.</summary>
        public double Altitude { get; set; }

        /// <summary>The distance in meters to the north of the airport coordinates.</summary>
        public double North { get; set; }

        /// <summary>Optional name of the point. Some names have special significance.</summary>
        public string Name { get; set; }
    }
}

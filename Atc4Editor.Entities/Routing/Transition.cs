﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

namespace Atc4Editor.Entities.Routing
{
    /// <summary>
    ///     A transition describes the enroute transition part of a Standard Instrument Departure (SID). It's one of the
    ///     routes from where the common route, described by a <see cref="Departure"/>, splits into multiple routes.
    /// </summary>
    public sealed class Transition : RoutePart
    {
        /// <summary>Creates a new transition.</summary>
        /// <param name="name">The name of the transition.</param>
        public Transition(string name) => Name = name;
    }
}

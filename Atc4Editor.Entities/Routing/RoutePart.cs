﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

namespace Atc4Editor.Entities.Routing
{
    /// <summary>All parts making up a route inherit from this class.</summary>
    public abstract class RoutePart
    {
        /// <summary>The name of the route part.</summary>
        public string Name { get; protected set; }

        /// <summary>Turn the route part into a readable string, for debugging purposes.</summary>
        /// <returns>A string that can be used by a human to identify the route part.</returns>
        public override string ToString() => Name;
    }
}

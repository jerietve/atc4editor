﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;

namespace Atc4Editor.Entities
{
    /// <summary>A place where an aircraft can park, like a gate or parking.</summary>
    public sealed class Spot
    {
        /// <summary>The name of the spot as used in the game.</summary>
        public string Name { get; set; }

        /// <summary>The general type of activity the spot is used for, such as passengers vs cargo.</summary>
        public string Usage { get; set; }

        /// <summary>The maximum size of aircraft that fits into this spot.</summary>
        public Model.Size Size { get; set; }

        /// <summary>Whether this spot has an air bridge.</summary>
        public bool Bridge { get; set; }

        /// <summary>The other spots which cannot be used when a maximum size aircraft occupies this spot.</summary>
        public ICollection<string> ConflictingSpots { get; set; }

        /// <summary>Turns the instance a readable string, for debugging purposes.</summary>
        /// <returns>A string that can be used by a human to identify the instance.</returns>
        public override string ToString() => Name;
    }
}

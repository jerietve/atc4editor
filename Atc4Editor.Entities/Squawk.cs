﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Globalization;
using Atc4Editor.Entities.Resources;

namespace Atc4Editor.Entities
{
    /// <summary>
    ///     An aircraft squawk code. A four digit number unique to an ATC area used by controllers to identify the
    ///     aircraft.
    /// </summary>
    public sealed class Squawk
    {
        private readonly int _squawk;

        /// <summary>Creates a new squawk.</summary>
        /// <param name="digit1">The first digit. Must be between zero and seven, both inclusive.</param>
        /// <param name="digit2">The second digit. Must be between zero and seven, both inclusive.</param>
        /// <param name="digit3">The third digit. Must be between zero and seven, both inclusive.</param>
        /// <param name="digit4">The fourth digit. Must be between zero and seven, both inclusive.</param>
        public Squawk(int digit1, int digit2, int digit3, int digit4)
        {
            if(digit1 < 0 || digit1 > 7) throw new ArgumentException(nameof(digit1));
            if(digit2 < 0 || digit2 > 7) throw new ArgumentException(nameof(digit2));
            if(digit3 < 0 || digit3 > 7) throw new ArgumentException(nameof(digit3));
            if(digit4 < 0 || digit4 > 7) throw new ArgumentException(nameof(digit4));

            _squawk = digit1 * 1000 + digit2 * 100 + digit3 * 10 + digit4;
        }

        public Squawk(string squawk)
        {
            if(string.IsNullOrEmpty(squawk)) throw new ArgumentNullException(nameof(squawk));

            if(!int.TryParse(squawk, out int squawkInt))
                throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, Strings.SquawkShouldBeNumbers, squawk),
                    nameof(squawk));

            int digit1 = squawkInt / 1000 % 10;
            int digit2 = squawkInt / 100 % 10;
            int digit3 = squawkInt / 10 % 10;
            int digit4 = squawkInt % 10;

            if(digit1 < 0 || digit1 > 7 || digit2 < 0 || digit2 > 7 || digit3 < 0 || digit3 > 7 || digit4 < 0 || digit4 > 7)
                throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, Strings.SquawkShouldBeNumbers, squawk),
                    nameof(squawk));

            _squawk = digit1 * 1000 + digit2 * 100 + digit3 * 10 + digit4;
        }

        /// <summary>Turns the instance a readable string, for debugging purposes.</summary>
        /// <returns>A string that can be used by a human to identify the instance.</returns>
        public override string ToString() => _squawk.ToString(CultureInfo.InvariantCulture);
    }
}

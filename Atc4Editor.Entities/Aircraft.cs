﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

namespace Atc4Editor.Entities
{
    /// <summary>Describes an aircraft type.</summary>
    public sealed class Aircraft
    {
        private string _icaoCode;

        /// <summary>The ICAO code of the aircraft type.</summary>
        public string IcaoCode
        {
            get => _icaoCode;
            set => _icaoCode = value.ToUpperInvariant();
        }

        /// <summary>Ordered list of ICAO codes of aircraft types most similar to this aircraft type.</summary>
        public string[] Alternatives { get; set; }
    }
}

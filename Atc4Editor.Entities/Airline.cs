﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

namespace Atc4Editor.Entities
{
    /// <summary>Airline information.</summary>
    public sealed class Airline
    {
        private string _iataCode;
        private string _icaoCode;

        /// <summary>Three character ICAO airline designator code uniquely describing this airline.</summary>
        public string IcaoCode
        {
            get => _icaoCode;
            set => _icaoCode = value.ToUpperInvariant();
        }

        /// <summary>Optional two character IATA airline designator describing this airline. Not unique.</summary>
        public string IataCode
        {
            get => _iataCode;
            set => _iataCode = value.ToUpperInvariant();
        }

        /// <summary>Full airline name.</summary>
        public string Name { get; set; }
    }
}

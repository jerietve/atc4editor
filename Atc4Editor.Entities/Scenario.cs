﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Collections.Generic;
using System.Globalization;
using Atc4Editor.Entities.Resources;

namespace Atc4Editor.Entities
{
    public sealed class Scenario
    {
        private int _month;
        private int _duration;
        public Scenario() => Movements = new List<Movement>();

        // Information section
        public string Creator { get; set; }
        public DateTime CreationDate { get; set; }
        public int Version { get; set; }
        public bool SavingEnabled { get; set; }
        public Airport Airport { get; set; }

        // General section
        public int Month
        {
            get => _month;
            set
            {
                if(value < 1 || value > 12)
                    throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.IncorrectValueForMonth, value));
                _month = value;
            }
        }

        public Time StartTime { get; set; }

        public int Duration
        {
            get => _duration;
            set
            {
                if(value < 1) throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.DurationMustBePositive, value));
                _duration = value;
            }
        }

        // TODO is there some maximum to the number or line length?
        public string[] Comments { get; set; }

        // Ship sections
        public List<Movement> Movements { get; set; }
    }
}

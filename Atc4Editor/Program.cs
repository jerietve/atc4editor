﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Diagnostics;
using System.IO;
using Atc4Editor.Core;
using Atc4Editor.Entities;
using Atc4Editor.Resources;

namespace Atc4Editor
{
    /// <summary>
    ///     The main class of the scenario creator. Handles command line input and global configuration. TODO also
    ///     contains a legacy attempt at replicating x.exe
    /// </summary>
    public class Program
    {
        private const char GroupSeparator = (char)29;

        public static string AirportInfoFolder = @"Configuration\AirportInfo";
        public static string AirportName = "RJFF";
        public static string Atc4Folder;

        /// <summary>Entry point of the application. Handles command line parameters to perform the user request.</summary>
        /// <param name="args">The arguments. Provide nothing to get usage information.</param>
        public static void Main(string[] args)
        {
            if(args.Length == 0)
            {
                PrintUsage();
                return;
            }

            switch(args[0])
            {
                case "a":
                case "analyze":
                    CallAnalyzeScenario(args);
                    return;
                case "c":
                case "create":
                    CallCreateScenario(args);
                    return;
                case "u":
                case "unpack":
                    if(args.Length != 2)
                    {
                        PrintUsage();
                        return;
                    }

                    DecodeSna(args[1]);
                    return;
                default:
                    PrintUsage();
                    return;
            }
        }

        private static void PrintUsage()
        {
            Console.WriteLine(Strings.Usage);
        }

        private static void CallAnalyzeScenario(string[] args)
        {
            int index = 1;
            string scenarioPath = null;

            while(index < args.Length)
            {
                switch(args[index++])
                {
                    case "-s":
                    case "--scenario":
                        if(!VerifyArgumentOrPrintUsage(args, index)) return;
                        scenarioPath = args[index];
                        continue;
                }

                bool VerifyArgumentOrPrintUsage(string[] strings, int i)
                {
                    if(i != strings.Length) return true;
                    PrintUsage();
                    return false;
                }
            }

            if(scenarioPath == null)
            {
                PrintUsage();
                return;
            }

            ScenarioAnalyzer.AnalyzeScenario(scenarioPath);
        }

        private static void CallCreateScenario(string[] args)
        {
            int index = 1;
            string outputPath = null, schedulePath = null, gamePath = null, fromTime = null, toTime = null;

            while(index < args.Length)
            {
                switch(args[index++])
                {
                    case "-o":
                    case "--output":
                        if(!VerifyArgumentOrPrintUsage(args, index)) return;
                        outputPath = args[index];
                        continue;
                    case "-s":
                    case "--schedule":
                        if(!VerifyArgumentOrPrintUsage(args, index)) return;
                        schedulePath = args[index];
                        continue;
                    case "-g":
                    case "--game":
                        if(!VerifyArgumentOrPrintUsage(args, index)) return;
                        gamePath = args[index];
                        continue;
                    case "-f":
                    case "--from-time":
                        if(!VerifyArgumentOrPrintUsage(args, index)) return;
                        fromTime = args[index];
                        continue;
                    case "-t":
                    case "--to-time":
                        if(!VerifyArgumentOrPrintUsage(args, index)) return;
                        toTime = args[index];
                        continue;
                }

                bool VerifyArgumentOrPrintUsage(string[] strings, int i)
                {
                    if(i != strings.Length) return true;
                    PrintUsage();
                    return false;
                }
            }

            if(outputPath == null || schedulePath == null || gamePath == null || fromTime == null || toTime == null)
            {
                PrintUsage();
                return;
            }

            Atc4Folder = gamePath;
            CreateScenario(outputPath, schedulePath, fromTime, toTime);
        }

        private static void CreateScenario(string outputPath, string schedulePath, string fromTime, string toTime)
        {
            Time startTime = new Time(fromTime);
            Time endTime = new Time(toTime);

            ScenarioCreator scenarioCreator = new ScenarioCreator(Atc4Folder, AirportInfoFolder, AirportName);
            scenarioCreator.CreateSchedule(outputPath, schedulePath, startTime, endTime);
        }

        private static void DecodeSna(string filePath)
        {
            // Read the entire file
            byte[] fileBytes = File.ReadAllBytes(filePath);

            // Create a text file next to the input file
            FileStream outFileStream = File.Open(filePath + ".txt", FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(outFileStream);

            int index = 0;

            // Get to the header
            while(index < fileBytes.Length && fileBytes[index] != (byte)'{') index++;

            while(index < fileBytes.Length)
            {
                if(index < fileBytes.Length - 1 && fileBytes[index] == (byte)'-' && fileBytes[index + 1] == (byte)'*')
                {
                    writer.WriteLine();
                    index += 2;
                    continue;
                }

                char c = ConvertChar(fileBytes[index++]);
                if(c == '[') writer.WriteLine();
                writer.Write(c);
            }

            writer.Flush();
        }

        private static int WriteHeader(byte[] fileBytes, int index, StreamWriter writer)
        {
            writer.Write('[');
            index++;
            byte b;
            while(index < fileBytes.Length && (b = fileBytes[index++]) != '}') writer.Write(ConvertChar(b));
            writer.WriteLine(']');
            return index;
        }

        private static int WriteSection(byte[] fileBytes, int index, StreamWriter writer)
        {
            // Make sure we're at a "-*", then skip over it
            while(index < fileBytes.Length - 1 && (fileBytes[index] != (byte)'-' || fileBytes[index + 1] != (byte)'*')) index++;
            index += 2;

            do
            {
                byte b;
                while(index < fileBytes.Length && (b = fileBytes[index++]) != GroupSeparator)
                    writer.Write(ConvertChar(b));

                writer.Write('=');

                while(index < fileBytes.Length - 1 && (fileBytes[index] != (byte)'-' || fileBytes[index + 1] != (byte)'*'))
                    writer.Write(ConvertChar(fileBytes[index++]));

                writer.WriteLine();
                index += 2;
            } while(index < fileBytes.Length && fileBytes[index] != (byte)'{');

            return index;
        }

        private static char ConvertChar(byte b)
        {
            switch(b)
            {
                case 0x00: return ' ';
                case 0x04: return '!';
                case 0x0C: return ',';
                case 0x0D: return '-';
                case 0x0E: return '.';
                case 0x0F: return '/';
                case 0xAA: return (char)0x00;
                case 0x1D: return '=';
                case 0x7B: return '[';
                case 0x7D: return ']';
                case 0x7F: return '_';
                case 0x2D: return '#';
                case 0x3A: return ':';
                case 0xFE: return (char)0x03;
            }

            if(b >= 0x10 && b < 0x1A) return (char)(b - 0x10 + '0');
            if(b >= 'A' && b <= 'Z') return (char)(b + 'a' - 'A');
            if(b >= 'a' && b <= 'z') return (char)(b - 'a' + 'A');

            Debug.WriteLine("No conversion available for character " + b);
            return (char)b;
            throw new NotImplementedException("No conversion available for character " + b);
        }
    }
}

﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using Atc4Editor.Core.Resources;
using Atc4Editor.Entities;
using Atc4Editor.Entities.Routing;

namespace Atc4Editor.Core
{
    public static class ScenarioReader
    {
        private static readonly Regex MovementRegex = new Regex(@"^\[SHIP\d{4}\]$");

        public static Scenario ReadScenario(string filePath)
        {
            string[] lines = File.ReadAllLines(filePath);
            for(int i = 0; i < lines.Length; i++) lines[i] = lines[i].Trim();

            Scenario scenario = new Scenario();

            int index = 0;

            while(index < lines.Length)
            {
                string line = lines[index];

                if(line.StartsWith(@"[INFORMATION]")) ReadInformationSection(scenario, lines, ref index);
                else if(MovementRegex.IsMatch(line)) scenario.Movements.Add(ReadMovement(lines, ref index));
                else if(line.StartsWith(@"[GENERAL]")) ReadGeneralSection(scenario, lines, ref index);
                else index++;
            }

            return scenario;
        }

        private static void ReadInformationSection(Scenario scenario, string[] lines, ref int index)
        {
            index++;

            while(index < lines.Length)
            {
                string line = lines[index];
                if(line.StartsWith("[")) break;

                (string key, string value) = GetKeyValuePair(index, line);

                try
                {
                    switch(key)
                    {
                        case "creator":
                            scenario.Creator = value;
                            break;
                        case "date":
                            if(!DateTime.TryParseExact(value, "yyyy/MM/dd", CultureInfo.InvariantCulture, DateTimeStyles.None,
                                out DateTime creationDate))
                                throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.ScenarioWrongDateFormat, value));
                            scenario.CreationDate = creationDate;
                            break;
                        case "version":
                            if(!int.TryParse(value, NumberStyles.None, CultureInfo.InvariantCulture, out int version))
                                throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.ScenarioWrongIntFormat, value));
                            scenario.Version = version;
                            break;
                        case "nosave":
                            if(value == "0") scenario.SavingEnabled = true;
                            else if(value == "1") scenario.SavingEnabled = false;
                            else
                                throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.BooleanValueMustBeZeroOrOne, key,
                                    value));
                            break;
                        case "port":
                            scenario.Airport = new Airport {IcaoCode = value};
                            break;
                        default:
                            throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, Strings.UnknownScenarioSetting, index,
                                key));
                    }
                }
                catch(Exception e)
                {
                    throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.ErrorReadingScenario, index, e.Message), e);
                }

                index++;
            }
        }

        private static Movement ReadMovement(string[] lines, ref int index)
        {
            int firstLine = index;
            index++;

            MovementFactory factory = new MovementFactory();
            Movement movement = new Movement();

            string route00 = null;
            string route01 = null;
            string route02 = null;
            string route03 = null;
            string route04 = null;

            while(index < lines.Length)
            {
                string line = lines[index];
                if(line.StartsWith("[")) break;

                (string key, string value) = GetKeyValuePair(index, line);

                try
                {
                    switch(key)
                    {
                        case "shipcode":
                            movement.ShipCode = value;
                            break;
                        case "callsign":
                            movement.CallSign = value;
                            break;
                        case "squawk":
                            movement.Squawk = new Squawk(value);
                            break;
                        case "runway":
                            movement.Runway = value;
                            break;
                        case "spot":
                            movement.Spot = value;
                            break;
                        case "icaocode":
                            movement.IcaoCode = value;
                            break;
                        case "depport":
                            movement.DepartureAirport = value;
                            break;
                        case "arrport":
                            movement.ArrivalAirport = value;
                            break;
                        case "start":
                            movement.Start = new Time(value);
                            break;
                        case "ready":
                            // TODO handle times like "1200.59"
                            movement.Ready = new Time(value.Substring(0, Math.Min(4, value.Length)));
                            break;
                        case "mode":
                            // TODO
                            break;
                        case "backcolor":
                            movement.Type = MovementFactory.ConvertBackColorToMovementType(value);
                            break;
                        case "textcolor":
                            // TODO
                            break;
                        case "string3":
                            // TODO
                            break;
                        case "string4":
                            // TODO
                            break;
                        case "skydirect":
                            // TODO this value is actually the direction from the airport probably
                            movement.InitialHeading = int.Parse(value);
                            break;
                        case "skydistance":
                            movement.InitialDistance = double.Parse(value);
                            break;
                        case "skyalt":
                            movement.InitialAltitude = int.Parse(value);
                            break;
                        case "skyias":
                            movement.InitialSpeed = int.Parse(value);
                            break;
                        case "skywpoint":
                            movement.InitialWaypoint = value;
                            break;
                        case "groundalt":
                            movement.GroundAlt = int.Parse(value);
                            break;
                        case "groundnode":
                            movement.GroundNode = value;
                            break;
                        case "groundpushback":
                            movement.GroundPushback = value == "1";
                            break;
                        case "groundhold":
                            // TODO
                            break;
                        case "groundhidespotin":
                            // TODO
                            break;
                        case "groundmidnode":
                            // TODO
                            break;
                        case "linkcount":
                            // TODO
                            break;
                        case "linkposition":
                            // TODO
                            break;
                        case "linkdeaprture":
                            // TODO
                            break;
                        case "route00":
                            route00 = value;
                            break;
                        case "route01":
                            route01 = value;
                            break;
                        case "route02":
                            route02 = value;
                            break;
                        case "route03":
                            route03 = value;
                            break;
                        case "route04":
                            route04 = value;
                            break;
                        case "section":
                            // TODO
                            break;
                        case "state":
                            // TODO
                            break;
                        case "extcolor":
                            // TODO
                            break;
                        case "ownchar":
                        case "tempspec":
                            //TODO and add to wiki
                            break;
                        default:
                            if(key.StartsWith("setupvalue"))
                            {
                                // TODO
                            }
                            else if(key.StartsWith("registration"))
                            {
                                // TODO
                            }
                            else if(key.StartsWith("event"))
                            {
                                // TODO
                            }
                            else
                            {
                                throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, Strings.UnknownScenarioSetting,
                                    index,
                                    key));
                            }

                            break;
                    }
                }
                catch(Exception e)
                {
                    throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.ErrorReadingScenario, index, e.Message), e);
                }

                index++;
            }

            if(route00 != null)
                // TODO handle route creation
                movement.Route = new Route(new Runway("RWY36"), new Departure("BLA"));

            try
            {
                factory.ValidateMovement(movement);
            }
            catch(Exception e)
            {
                throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.MovementNotValid, firstLine + 1, e.Message), e);
            }

            return movement;
        }

        private static void ReadGeneralSection(Scenario scenario, string[] lines, ref int index)
        {
            index++;

            while(index < lines.Length)
            {
                string line = lines[index];
                if(line.StartsWith("[")) break;

                (string key, string value) = GetKeyValuePair(index, line);

                try
                {
                    switch(key)
                    {
                        case "month":
                            if(!int.TryParse(value, NumberStyles.None, CultureInfo.InvariantCulture, out int month))
                                throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.ScenarioWrongIntFormat, value));
                            scenario.Month = month;
                            break;
                        case "basetime":
                            scenario.StartTime = new Time(value);
                            break;
                        case "baserange":
                            if(!int.TryParse(value, NumberStyles.None, CultureInfo.InvariantCulture, out int duration))
                                throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.ScenarioWrongIntFormat, value));
                            scenario.Duration = duration;
                            break;
                        case "commentline":
                            if(!int.TryParse(value, NumberStyles.None, CultureInfo.InvariantCulture, out int numberOfLines))
                                throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.ScenarioWrongIntFormat, value));
                            scenario.Comments = new string[numberOfLines];
                            break;
                        default:
                            if(key.StartsWith("comment"))
                            {
                                if(int.TryParse(key.Substring("comment".Length), NumberStyles.None, CultureInfo.InvariantCulture,
                                    out int commentNumber))
                                {
                                    if(scenario.Comments == null)
                                        throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.CommentBeforeCommentLine));
                                    if(scenario.Comments.Length < commentNumber)
                                        throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.CommentLineNumberOutOfRange,
                                            commentNumber, scenario.Comments.Length));
                                    scenario.Comments[commentNumber] = value;
                                }
                            }
                            else
                            {
                                throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, Strings.UnknownScenarioSetting,
                                    index,
                                    key));
                            }

                            break;
                    }
                }
                catch(Exception e)
                {
                    throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.ErrorReadingScenario, index, e.Message), e);
                }

                index++;
            }
        }

        private static (string key, string value) GetKeyValuePair(int index, string line)
        {
            string[] keyValuePair = line.Split(new[] {'='}, 2);

            if(keyValuePair.Length != 2)
                throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.ScenarioLineWrongFormat, index + 1, line));
            return (keyValuePair[0], keyValuePair[1]);
        }
    }
}

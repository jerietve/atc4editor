﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Atc4Editor.Entities;
using Atc4Editor.Entities.Routing;

namespace Atc4Editor.Core
{
    /// <summary>Writes out the scenario text file.</summary>
    internal class ScenarioWriter
    {
        private readonly int _month;
        private readonly IList<Movement> _movements;
        private readonly int _runTime;
        private readonly Time _startTime;

        /// <summary>Initializes a new scenario writer. Used to create a single scenario.</summary>
        /// <param name="startDateTime">The start time of the scenario.</param>
        /// <param name="runTime">The runtime of the scenario in minutes.</param>
        public ScenarioWriter(DateTime startDateTime, int runTime)
        {
            _month = startDateTime.Month;
            _startTime = new Time(startDateTime.Hour, startDateTime.Minute);
            _runTime = runTime;
            _movements = new List<Movement>();
        }

        /// <summary>Adds a movement to the scenario. Movements are numbered and written in the order they are added.</summary>
        /// <param name="movement"></param>
        public void AddMovement(Movement movement) => _movements.Add(movement);

        /// <summary>Validates and then writes out the scenario to a file.</summary>
        /// <param name="path">The file to write the scenario to.</param>
        public void WriteToFile(string path)
        {
            ValidateMovements();

            FileStream outFileStream = File.Open(path, FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(outFileStream);

            WriteInformationElement(writer);
            WriteGeneralElement(writer);
            WriteWeatherElement(writer);

            int index = 0;
            foreach(Movement movement in _movements) WriteMovement(movement, index++, writer);

            WriteAtc4Element(writer);
            writer.Flush();
        }

        private void ValidateMovements()
        {
            for(int outer = 0; outer < _movements.Count; outer++)
            {
                for(int inner = 0; inner < _movements.Count; inner++)
                {
                    if(inner == outer) continue;
                    if(_movements[outer].CallSign == _movements[inner].CallSign)
                        throw new ArgumentException("Multiple movements with call sign " + _movements[outer].CallSign);
                }

                if(_movements[outer].Start < _startTime)
                    throw new ArgumentException("Movement starting before scenario");
            }
        }

        private static void WriteInformationElement(StreamWriter writer)
        {
            writer.WriteLine("[INFORMATION]");
            writer.WriteLine("creator=ATC4Editor");
            writer.WriteLine("date=" + DateTime.Now.ToString("yyyy/MM/dd"));
            writer.WriteLine("version=2100");
            writer.WriteLine("nosave=0");
        }

        private static void WriteWeatherElement(StreamWriter writer)
        {
            writer.WriteLine("[WEATHER]");
            writer.WriteLine("qnh=0,2992,10,2993,20,2994,30,2993,40,2992,50,2991,60,2992");
            writer.WriteLine("visibleend=0,34333,10,33037,20,32333,30,33407,40,34481,50,34259,60,32407");
            writer.WriteLine("visiblestart=0,0,10,0,20,0,30,0,40,0,50,0,60,0");
            writer.WriteLine("wind1speed=0,3,10,3,20,2,30,3,40,2,50,0,60,0");
            writer.WriteLine("wind1direct=0,0,0,0,0,0");
            writer.WriteLine("wind2speed=0,3,10,3,20,2,30,3,40,2,50,0,60,0");
            writer.WriteLine("wind2direct=0,0,0,0,0,0");
            writer.WriteLine("rainfall=0,0,60,0");
            writer.WriteLine("snowfall=0,0,60,0");
            writer.WriteLine("lightrate=0,100,10,100,20,100,30,100,40,100,50,100,60,100");
            writer.WriteLine("cloud=NearlyOvercast");
            writer.WriteLine("cloudheight=4000");
        }

        private void WriteGeneralElement(StreamWriter writer)
        {
            writer.WriteLine("[GENERAL]");
            writer.WriteLine("month=" + _month);
            writer.WriteLine("basetime=" + _startTime);
            writer.WriteLine("baserange=" + _runTime);
            writer.WriteLine("commentline=1");
            writer.WriteLine("comment00=ATC4Editor");
        }

        private static void WriteMovement(Movement movement, int index, StreamWriter writer)
        {
            writer.WriteLine("[SHIP" + index.ToString().PadLeft(4, '0') + "]");
            writer.WriteLine("shipcode=" + movement.ShipCode);
            writer.WriteLine("callsign=" + movement.CallSign);
            writer.WriteLine("squawk=" + movement.Squawk);
            if(movement.Type != Movement.MovementType.Stationary) writer.WriteLine("runway=" + movement.Runway);
            writer.WriteLine("spot=" + movement.Spot);
            writer.WriteLine("icaocode=" + movement.IcaoCode);

            if(movement.Type == Movement.MovementType.Departing || movement.Type == Movement.MovementType.Arriving)
            {
                writer.WriteLine("depport=" + movement.DepartureAirport);
                writer.WriteLine("arrport=" + movement.ArrivalAirport);
            }

            writer.WriteLine("start=" + movement.Start);
            if(movement.Type != Movement.MovementType.Stationary) writer.WriteLine("ready=" + movement.Ready);
            writer.WriteLine("mode=" + GetMode(movement));
            writer.WriteLine("backcolor=" + GetBackgroundColor(movement));
            writer.WriteLine("textcolor=0");
            if(movement.Type == Movement.MovementType.Departing || movement.Type == Movement.MovementType.Arriving)
                writer.WriteLine("string3=" + GetString3(movement));
            writer.WriteLine("skydirect=0");
            writer.WriteLine("skydistance=0");
            writer.WriteLine("skyalt=0");
            writer.WriteLine("skyias=0");
            if(movement.InitialWaypoint != null) writer.WriteLine("skywpoint=" + movement.InitialWaypoint);
            writer.WriteLine("groundalt=0");
            // TODO groundnode
            writer.WriteLine("groundpushback=" + (movement.GroundPushback ? 1 : 0));
            writer.WriteLine("groundhold=0");
            writer.WriteLine("groundhidespotin=0");
            writer.WriteLine("linkcount=0");
            writer.WriteLine("linkposition=0");
            writer.WriteLine("linkdeaprture=0");

            if(movement.Type == Movement.MovementType.Departing || movement.Type == Movement.MovementType.Arriving)
            {
                IList<RoutePart> parts = movement.Route.RouteParts;
                for(int i = 0; i < parts.Count; i++)
                    writer.WriteLine("route" + i.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0') + "=" + parts[i]);
            }

            writer.WriteLine("registration1=" + movement.Registration);

            if(movement.Section != Section.Default)
            {
                writer.WriteLine("section=" + movement.Section.ToString().ToUpperInvariant());
                writer.WriteLine("state=" + GetStateString(movement.State));
            }

            writer.WriteLine("extcolor=0x00000000");
            writer.WriteLine("setupvalue=0");
            writer.WriteLine("setupvalue2=0");
            writer.WriteLine("setupvalue3=0");
            writer.WriteLine("setupvalue4=0");
        }

        private static void WriteAtc4Element(StreamWriter writer)
        {
            writer.WriteLine("[ATC4]");
            writer.WriteLine("autosection=DEL");
            writer.WriteLine("autoskip=0");
            writer.WriteLine("operationoffset=15");
            writer.WriteLine("atcwait=1000");
            writer.WriteLine("weathermark=0");
            writer.WriteLine("base_a=9");
            writer.WriteLine("base_b=1000");
            writer.WriteLine("autobrake=0");
            writer.WriteLine("level_s=12");
            writer.WriteLine("level_a=11");
            writer.WriteLine("level_b=10");
            writer.WriteLine("level_c=9");
            writer.WriteLine("level_d=0");
            writer.WriteLine("level_e=0");
            writer.WriteLine("level_star=4");
            writer.WriteLine("lastsectiondep=DEP");
            writer.WriteLine("lastsectionapp=GND");
            writer.WriteLine("productid=WTLF003");
        }

        private static int GetMode(Movement movement)
        {
            // TODO
            return 21;
            switch(movement.Type)
            {
                case Movement.MovementType.Departing:
                    return movement.AppearsFromStart ? 22 : 21;
                case Movement.MovementType.Arriving:
                    return movement.AppearsFromStart ? 11 : 12;
                case Movement.MovementType.Towing:
                    return 21;
                case Movement.MovementType.Stationary:
                    return 21;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static int GetBackgroundColor(Movement movement)
        {
            switch(movement.Type)
            {
                case Movement.MovementType.Departing:
                    return 1;
                case Movement.MovementType.Arriving:
                    return 2;
                case Movement.MovementType.Towing:
                    return 3;
                case Movement.MovementType.Stationary:
                    return 4;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static string GetString3(Movement movement)
        {
            switch(movement.Type)
            {
                case Movement.MovementType.Departing:
                    return movement.ArrivalAirport;
                case Movement.MovementType.Arriving:
                    return movement.DepartureAirport;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static string GetStateString(Movement.MovementState state)
        {
            switch(state)
            {
                case Movement.MovementState.SnaPushbackStart:
                    return "%SNA_PUSHBACK_START";
                case Movement.MovementState.SnaPushbackEnd:
                    return "SNA_PUSHBACK_END";
                case Movement.MovementState.SnaTaxiStart:
                    return "%SNA_TAXI_START";
                case Movement.MovementState.TaxiLoop:
                    return "TAXI_LOOP";
                case Movement.MovementState.SnaHoldShort:
                    return "%SNA_HOLD_SHORT";
                case Movement.MovementState.SnaTakeoff:
                    return "SNA_TAKEOFF";
                case Movement.MovementState.SnaTakeoffAtc:
                    return "SNA_TAKEOFF_ATC";
                case Movement.MovementState.Takeoff:
                    return "TAKEOFF";
                case Movement.MovementState.Approach:
                    return "APPROACH";
                case Movement.MovementState.VisualApproachWait:
                    return "VISUALAPP_WAIT";
                case Movement.MovementState.SnaFinal:
                    return "SNA_FINAL";
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }
    }
}

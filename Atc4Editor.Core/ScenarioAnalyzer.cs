﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Collections.Generic;
using System.Linq;
using Atc4Editor.Entities;

namespace Atc4Editor.Core
{
    public static class ScenarioAnalyzer
    {
        public static void AnalyzeScenario(string scenarioPath)
        {
            Scenario scenario = ScenarioReader.ReadScenario(scenarioPath);
            WriteModelInfo(scenario);
        }

        private static void WriteModelInfo(Scenario scenario)
        {
            Dictionary<string, int> shipOccurrences = new Dictionary<string, int>();
            foreach(Movement movement in scenario.Movements)
            {
                if(!shipOccurrences.ContainsKey(movement.ShipCode)) shipOccurrences.Add(movement.ShipCode, 0);
                shipOccurrences[movement.ShipCode]++;
            }

            Console.WriteLine("Models in this scenario:");
            Console.WriteLine();
            Console.WriteLine("#  | Name");
            foreach(KeyValuePair<string, int> shipOccurrence in shipOccurrences)
                Console.WriteLine($"{shipOccurrence.Value.ToString().PadLeft(2)} | {shipOccurrence.Key}");

            WritePairs(new[]
            {
                ("Number of departures:", scenario.Movements.Count(m => m.Type == Movement.MovementType.Departing).ToString()),
                ("Number of arrivals:", scenario.Movements.Count(m => m.Type == Movement.MovementType.Arriving).ToString()),
                ("Number of ground movements:", scenario.Movements.Count(m => m.Type == Movement.MovementType.Towing).ToString()),
                ("Number of stationary:", scenario.Movements.Count(m => m.Type == Movement.MovementType.Stationary).ToString()),
                ("Planes in scene at start:", scenario.Movements.Count(m => m.AppearsFromStart).ToString())
            });

            WriteHeading("Planes in order of appearance:");
            List<List<string>> movementList = scenario.Movements.OrderBy(m => m.Ready)
                .Select(m => new List<string> {m.Start.ToString(), m.Ready.ToString(), m.Type.ToString(), m.ShipCode}).ToList();
            movementList.Insert(0, new List<string> {"Start", "Ready", "Type", "Model"});
            WriteTable(movementList, true);
        }

        private static void WritePairs((string left, string right)[] pairs)
        {
            int longestLeft = pairs.Select(pair => pair.left.Length).Max();
            foreach((string left, string right) pair in pairs)
                Console.WriteLine($"{pair.left.PadRight(longestLeft)} | {pair.right}");
            Console.WriteLine();
        }

        private static void WriteTable(IReadOnlyList<List<string>> table, bool hasHeaders)
        {
            int[] widths = new int[table[0].Count];
            for(int i = 0; i < widths.Length; i++) widths[i] = table.Select(row => row[i].Length).Max();

            string separatorLine = "".PadRight(widths.Sum() + widths.Length * 3 + 1, '-');
            Console.WriteLine(separatorLine);
            WriteRow(table[0], widths);
            if(hasHeaders) Console.WriteLine(separatorLine);
            for(int i = 1; i < table.Count; i++) WriteRow(table[i], widths);
            Console.WriteLine(separatorLine);
        }

        private static void WriteRow(IReadOnlyList<string> values, IReadOnlyList<int> widths)
        {
            Console.Write(@"|");
            for(int i = 0; i < values.Count; i++) Console.Write(@" " + values[i].PadRight(widths[i]) + @" |");
            Console.WriteLine();
        }

        private static void WriteHeading(string heading)
        {
            Console.WriteLine(heading);
            Console.WriteLine();
        }
    }
}

﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Globalization;
using Atc4Editor.Core.Resources;
using Atc4Editor.Entities;
using Atc4Editor.Entities.Routing;

namespace Atc4Editor.Core
{
    /// <summary>Helps prepare a single movement by validating its properties.</summary>
    internal sealed class MovementFactory
    {
        private readonly Random _random = new Random();
        private Movement _movement;

        /// <summary>Starts a new <see cref="Movement"/> creation.</summary>
        /// <param name="shipCode">The <see cref="Movement.ShipCode"/>.</param>
        /// <param name="icaoCode">The <see cref="Movement.IcaoCode"/>.</param>
        /// <param name="registration">The <see cref="Movement.Registration"/>.</param>
        /// <param name="callSign">The <see cref="Movement.CallSign"/>.</param>
        /// <param name="type">The <see cref="Movement.Type"/>.</param>
        /// <param name="spot">The <see cref="Movement.Spot"/>.</param>
        /// <param name="appearsFromStart">The <see cref="Movement.AppearsFromStart"/> value.</param>
        public void Initialize(string shipCode, string icaoCode, string registration, string callSign,
            Movement.MovementType type, string spot, bool appearsFromStart)
        {
            if(string.IsNullOrWhiteSpace(shipCode)) throw new ArgumentNullException(nameof(shipCode));
            if(string.IsNullOrWhiteSpace(icaoCode)) throw new ArgumentNullException(nameof(icaoCode));
            if(string.IsNullOrWhiteSpace(registration)) throw new ArgumentNullException(nameof(registration));
            if(string.IsNullOrWhiteSpace(callSign)) throw new ArgumentNullException(nameof(callSign));
            if(string.IsNullOrWhiteSpace(spot)) throw new ArgumentNullException(nameof(spot));
            if(type == Movement.MovementType.Stationary && !appearsFromStart)
                throw new ArgumentException("Stationary aircraft must appear from the start.");

            _movement = new Movement
            {
                ShipCode = shipCode,
                CallSign = callSign,
                Spot = spot,
                Type = type,
                IcaoCode = icaoCode,
                AppearsFromStart = appearsFromStart,
                Registration = registration,
                GroundPushback = type == Movement.MovementType.Towing || type == Movement.MovementType.Departing
            };
        }

        /// <summary>Sets the <see cref="Movement.Squawk"/>.</summary>
        /// <param name="squawk">The squawk for the movement.</param>
        public void SetSquawk(Squawk squawk) => _movement.Squawk = squawk ?? throw new ArgumentNullException(nameof(squawk));

        /// <summary>Sets the <see cref="Movement.DepartureAirport"/>.</summary>
        /// <param name="airportIcaoCode">The ICAO code of the departure airport.</param>
        public void SetDepartureAirport(string airportIcaoCode)
        {
            ValidateAirportCode(airportIcaoCode);
            _movement.DepartureAirport = airportIcaoCode;
        }

        /// <summary>Sets the <see cref="Movement.ArrivalAirport"/>.</summary>
        /// <param name="airportIcaoCode">The ICAO code of the arrival airport.</param>
        public void SetArrivalAirport(string airportIcaoCode)
        {
            ValidateAirportCode(airportIcaoCode);
            _movement.ArrivalAirport = airportIcaoCode;
        }

        /// <summary>Sets the <see cref="Movement.Start"/>.</summary>
        /// <param name="startTime">The movement start time.</param>
        public void SetStartTime(Time startTime) => _movement.Start = startTime;

        /// <summary>Sets the <see cref="Movement.Ready"/>.</summary>
        /// <param name="readyTime">The movement ready time.</param>
        public void SetReadyTime(Time readyTime) => _movement.Ready = readyTime;

        // TODO add way to set initial heading, distance, altitude, speed

        /// <summary>Sets the <see cref="Movement.InitialWaypoint"/>.</summary>
        /// <param name="waypoint">The name of the initial waypoint.</param>
        public void SetInitialWaypoint(string waypoint) =>
            _movement.InitialWaypoint = waypoint ?? throw new ArgumentNullException(nameof(waypoint));

        /// <summary>Sets the <see cref="Movement.GroundPushback"/>.</summary>
        /// <param name="pushback">Whether the movement requires pushback.</param>
        public void SetPushback(bool pushback) => _movement.GroundPushback = pushback;

        /// <summary>Sets the <see cref="Movement.Route"/> and <see cref="Movement.Runway"/>.</summary>
        /// <param name="route">The route the movement will follow.</param>
        public void SetRoute(Route route)
        {
            _movement.Route = route ?? throw new ArgumentNullException(nameof(route));
            foreach(RoutePart routePart in route.RouteParts)
                if(routePart is Runway runway)
                {
                    _movement.Runway = runway.ToString();
                    return;
                }
        }

        /// <summary>Sets the <see cref="Movement.Section"/>.</summary>
        /// <param name="section">The initial section of the movement at scenario start.</param>
        public void SetSection(Section section) => _movement.Section = section;

        /// <summary>Sets the <see cref="Movement.State"/>.</summary>
        /// <param name="state">The initial state of the movement at scenario start.</param>
        public void SetState(Movement.MovementState state) => _movement.State = state;

        /// <summary>Validate all input added since calling <see cref="Initialize"/> and return the <see cref="Movement"/>.</summary>
        /// <returns>A valid movement, or an exception.</returns>
        public Movement Finish()
        {
            ValidateMovement();
            Movement returning = _movement;
            _movement = null;
            return returning;
        }

        /// <summary>Checks whether a movement has any issues, like missing or incorrect (combinations of) values.</summary>
        /// <param name="movement">The movement to check.</param>
        public void ValidateMovement(Movement movement)
        {
            _movement = movement;
            ValidateMovement();
        }

        /// <summary>Converts a background color from a scenario file into a movement type.</summary>
        /// <param name="backColor">The background color to convert.</param>
        /// <returns>The converted background color.</returns>
        internal static Movement.MovementType ConvertBackColorToMovementType(string backColor)
        {
            switch(backColor)
            {
                case "1": return Movement.MovementType.Departing;
                case "2": return Movement.MovementType.Arriving;
                case "3": return Movement.MovementType.Towing;
                case "4": return Movement.MovementType.Stationary;
                default: throw new Exception(string.Format(CultureInfo.CurrentCulture, Strings.UnknownBackgroundColor, backColor));
            }
        }

        private void ValidateMovement()
        {
            // Even stationary aircraft have a squawk for some reason
            if(_movement.Squawk == null) _movement.Squawk = GenerateRandomSquawk();

            if(_movement.Type != Movement.MovementType.Stationary)
            {
                if(string.IsNullOrWhiteSpace(_movement.Runway))
                    throw new ArgumentNullException(nameof(_movement.Runway));
                if(_movement.Start == default) // TODO what if the flight is at midnight?
                    throw new ArgumentNullException(nameof(_movement.Start));
                if(_movement.Ready == default) // TODO what if the flight is at midnight?
                    throw new ArgumentNullException(nameof(_movement.Ready));

                // TODO
                //if(_movement.Type == Movement.MovementType.Departing && _movement.AppearsFromStart)
                //{
                //    if(_movement.Start < _movement.Ready)
                //        throw new ArgumentException(
                //            "Departing aircraft already on the move must have a ready time before the start time");
                //}
                //else if(_movement.Start > _movement.Ready)
                //    throw new ArgumentException("Movement is ready before it starts.");
            }

            if(_movement.Type == Movement.MovementType.Arriving || _movement.Type == Movement.MovementType.Departing)
            {
                if(_movement.DepartureAirport == null)
                    throw new ArgumentNullException(_movement.DepartureAirport);
                if(_movement.ArrivalAirport == null)
                    throw new ArgumentNullException(_movement.ArrivalAirport);
            }

            if(_movement.Type == Movement.MovementType.Arriving && !_movement.AppearsFromStart
                                                                && _movement.InitialWaypoint == null && (_movement.InitialHeading == null ||
                                                                                                         _movement.InitialDistance == null))
                throw new ArgumentNullException(nameof(_movement.InitialWaypoint),
                    "Aircraft arriving later must have an initial waypoint or location in the sky.");

            if((_movement.Type == Movement.MovementType.Departing || _movement.Type == Movement.MovementType.Arriving)
               && _movement.Route == null)
                throw new ArgumentException("Route must be set for arriving and departing aircraft.");

            if(_movement.Section != Section.Default && _movement.State == Movement.MovementState.Default)
                throw new ArgumentException("Aircraft in a non default section must have a state.");

            if(_movement.State != Movement.MovementState.Default && _movement.Section == Section.Default)
                throw new ArgumentException("Aircraft in the default section cannot have a state.");

            // lots of TODO here all over this class. Could add a lot more sanity checks.
        }

        private Squawk GenerateRandomSquawk() =>
            new Squawk(_random.Next(2, 8), _random.Next(2, 8), _random.Next(2, 8), _random.Next(2, 8));

        private void ValidateAirportCode(string airportIcaoCode)
        {
            if(airportIcaoCode == null) throw new ArgumentNullException(nameof(airportIcaoCode));
            if(airportIcaoCode.Length != 4)
                throw new ArgumentException("Invalid ICAO airport code: " + airportIcaoCode + ". Must be four characters.");
        }
    }
}

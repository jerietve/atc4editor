﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Collections.Generic;
using System.Linq;
using Atc4Editor.Entities;
using Atc4Editor.Entities.Routing;

namespace Atc4Editor.Core.Integration
{
    /// <summary>Creates routes for scenarios based on the route files of the game.</summary>
    internal sealed class Router
    {
        //private List<Route> _departureRoutes;
        //private ISet<RoutePart> _routeParts;
        private readonly IDictionary<string, ISet<string>> _areas;
        private readonly IDictionary<string, string[]> _departureRoutes;
        private readonly ISet<RouteTable> _routeTables;

        /// <summary>Initializes the router.</summary>
        public Router(string gameFolder, string airportIcaoCode)
        {
            //this._departureRoutes = new List<Route>();
            //this._routeParts = InfoReader.ReadRouteParts();
            _areas = InfoReader.ReadAreas(gameFolder, airportIcaoCode);
            _departureRoutes = InfoReader.ReadDepartureRoutes(gameFolder, airportIcaoCode);
            _routeTables = InfoReader.ReadRouteTables(gameFolder, airportIcaoCode);
        }

        /// <summary>Get the departure route for a certain destination.</summary>
        /// <param name="destinationIcaoCode">The destination airport its ICAO code.</param>
        /// <param name="departureTime">The departure time of day.</param>
        /// <returns>A departure route satisfying the requirements.</returns>
        public Route GetDepartureRoute(string destinationIcaoCode, Time departureTime)
        {
            // Find the area the airport is in
            string area = _areas.FirstOrDefault(a => a.Value.Contains(destinationIcaoCode)).Key;

            if(area == null) throw new Exception("Airport " + destinationIcaoCode + " not found in any area.");

            // Find the departure table that applies
            RouteTable departureTable = _routeTables.First(t =>
                t.TableType == RouteTable.RouteTableType.Departure && t.StartTime <= departureTime && t.EndTime >= departureTime);

            // Find the first route for the area in the departure table
            string departureRouteName = departureTable.AreaRoutes[area][0];

            // Find the departure route details
            string[] routeParts = _departureRoutes[departureRouteName];

            // Create the route
            Runway runway = new Runway(routeParts[0]);
            Departure departure = new Departure(routeParts[1]);
            Transition transition = new Transition(routeParts[2]);
            if(routeParts.Length == 4)
            {
                // TODO this is not actually a transition. What does DM stand for?
                RoutePart dm = new Transition(routeParts[3]);
                return new Route(runway, departure, transition, dm);
            }

            return new Route(runway, departure, transition);
        }
    }
}

﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Atc4Editor.Entities;
using Atc4Editor.Entities.Routing;

namespace Atc4Editor.Core
{
    /// <summary>Reading of files.</summary>
    public static class InfoReader
    {
        private static readonly Regex NameRegex = new Regex(@"^\s*name\s*=\s*([\w-]+)\s*$");
        private static readonly Regex AreaRegex = new Regex(@"^\s*\[(AREA_\w+)\]\s*$");
        private static readonly Regex AreaAirportRegex = new Regex(@"^\s*(\w{4})=\w+\s*$");

        /// <summary>Reads the taxi.csv file of the airport into a list of spots.</summary>
        /// <returns>The spots available at the airport.</returns>
        public static IList<Spot> ReadSpots(string gameFolder, string airportInfoFolder, string airportIcaoCode)
        {
            string taxiCsvPath = Path.Combine(gameFolder, "PORT", airportIcaoCode, "GROUND", "taxi.csv");
            string spotCsvPath = Path.Combine(airportInfoFolder, airportIcaoCode, "spots.csv");

            string[] taxiLines = File.ReadAllLines(taxiCsvPath);
            string[] spotLines = File.ReadAllLines(spotCsvPath);

            IList<string> existingSpots = new List<string>();
            IList<Spot> spots = new List<Spot>();
            foreach(string taxiLine in taxiLines)
            {
                string[] items = taxiLine.Split(',');
                string name = items[1];
                if(name.StartsWith("SPOT", StringComparison.Ordinal)) existingSpots.Add(name);
            }

            foreach(string spotLine in spotLines)
            {
                string[] items = spotLine.Split(',');
                string name = items[0];
                string works = items[1];
                if(works != "TRUE" || !existingSpots.Contains(name)) continue;

                Spot spot = new Spot
                {
                    Name = name,
                    Usage = items[2],
                    Size = ParseEnum<Model.Size>(items[3]),
                    Bridge = items[4] == "TRUE"
                };

                if(items.Length > 5)
                {
                    string[] conflictingSpots = items[5].Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries);
                    if(conflictingSpots.Length > 0) spot.ConflictingSpots = conflictingSpots;
                }

                spots.Add(spot);
            }

            return spots;
        }

        /// <summary>Reads the airlines.csv file. See the wiki.</summary>
        /// <returns>A list of airlines that exist in the world.</returns>
        public static IList<Airline> ReadAirlines(string airportInfoFolder)
        {
            string airlinesCsvPath = Path.Combine(airportInfoFolder, "airlines.csv");
            List<Airline> airlines = new List<Airline>();

            string[] lines = File.ReadAllLines(airlinesCsvPath);

            for(int index = 1; index < lines.Length; index++)
            {
                string line = lines[index];
                string[] items = line.Split(',');
                AssertCsvCount(3, items.Length, airlinesCsvPath, line);

                Airline airline = new Airline {IcaoCode = items[0], IataCode = items[1], Name = items[2]};
                airlines.Add(airline);
            }

            return airlines;
        }

        /// <summary>Reads the models.csv file. See the wiki.</summary>
        /// <returns>A list of models available to the scenario creator.</returns>
        public static IList<Model> ReadModels(string airportInfoFolder)
        {
            string modelsCsvPath = Path.Combine(airportInfoFolder, "models.csv");
            List<Model> models = new List<Model>();

            string[] lines = File.ReadAllLines(modelsCsvPath);

            for(int index = 1; index < lines.Length; index++)
            {
                string line = lines[index];
                string[] items = line.Split(',');
                AssertCsvCount(7, items.Length, modelsCsvPath, line);
                Model model = new Model
                {
                    Folder = items[0],
                    AircraftIcaoCode = items[1],
                    AirlineIcaoCode = items[2],
                    UniqueLivery = items[3] == "TRUE",
                    AircraftType = ParseEnum<Model.Type>(items[4]),
                    AircraftSize = ParseEnum<Model.Size>(items[5])
                };
                models.Add(model);
            }

            return models;
        }

        /// <summary>Reads the aircraft.csv file. See the wiki.</summary>
        /// <returns>A list of aircraft types that exist in the world.</returns>
        public static IList<Aircraft> ReadAircraft(string airportInfoFolder)
        {
            string aircraftCsvPath = Path.Combine(airportInfoFolder, "aircraft.csv");
            List<Aircraft> aircraftList = new List<Aircraft>();

            string[] lines = File.ReadAllLines(aircraftCsvPath);

            for(int index = 1; index < lines.Length; index++)
            {
                string line = lines[index];
                string[] items = line.Split(',');
                AssertCsvCount(3, items.Length, aircraftCsvPath, line);
                Aircraft aircraft = new Aircraft {IcaoCode = items[0], Alternatives = items[2].Split(';')};
                aircraftList.Add(aircraft);
            }

            return aircraftList;
        }

        /// <summary>Reads the flightradar_aircraft_conversion.csv file. See the wiki.</summary>
        /// <returns>A dictionary for converting Flightradar24.com aircraft codes into ICAO codes.</returns>
        public static IDictionary<string, string> ReadFlightradarAircraftConversion(string airportInfoFolder)
        {
            string conversionCsvPath = Path.Combine(airportInfoFolder, "flightradar_aircraft_conversion.csv");
            Dictionary<string, string> conversions = new Dictionary<string, string>();

            string[] lines = File.ReadAllLines(conversionCsvPath);

            for(int index = 1; index < lines.Length; index++)
            {
                string line = lines[index];
                string[] items = line.Split(',');
                AssertCsvCount(2, items.Length, conversionCsvPath, line);
                conversions.Add(items[0], items[1]);
            }

            return conversions;
        }

        /// <summary>Reads the airports.csv file. See the wiki.</summary>
        /// <returns>A list of airports that exist in the world.</returns>
        public static IList<Airport> ReadAirports(string airportInfoFolder)
        {
            string airportsCsvPath = Path.Combine(airportInfoFolder, "airports.csv");
            List<Airport> airports = new List<Airport>();

            string[] lines = File.ReadAllLines(airportsCsvPath);

            for(int index = 1; index < lines.Length; index++)
            {
                string line = lines[index];
                string[] items = line.Split(',');
                AssertCsvCount(5, items.Length, airportsCsvPath, line);
                Airport airport = new Airport
                    {IataCode = items[0], IcaoCode = items[1]};
                airports.Add(airport);
            }

            return airports;
        }

        // TODO make this private, move file reading into this class
        public static void AssertCsvCount(int expectedLength, int actualLength, string filePath, string line)
        {
            if(actualLength != expectedLength)
                throw new Exception(
                    $"Found line with {actualLength} instead of {expectedLength} comma separated values in file {filePath}: {line}");
        }

        private static T ParseEnum<T>(string value) => (T)Enum.Parse(typeof(T), value);

        internal static ISet<RoutePart> ReadRouteParts(string gameFolder, string airportIcaoCode)
        {
            string routesPath = Path.Combine(gameFolder, "PORT", airportIcaoCode, "ROUTE");

            List<string> routePartFileNames = Directory.EnumerateFiles(routesPath).ToList();
            ISet<RoutePart> routeParts = new HashSet<RoutePart>();

            foreach(string fileName in routePartFileNames)
            {
                string[] lines = File.ReadAllLines(fileName);

                string line = lines[0].Trim();
                if(line != "[HEADER]") throw new Exception("Expected route file to start with header: " + fileName);

                Match nameMatch = NameRegex.Match(lines[1]);
                if(!nameMatch.Success) throw new Exception("Expected the header to start with the name in route file " + fileName);

                string name = nameMatch.Groups[1].Captures[0].Value;
                switch(name.Substring(0, 3))
                {
                    case "AA_":
                        break;
                    case "AP_":
                        break;
                    case "AR_":
                        break;
                    case "DM_":
                        break;
                    case "DP_":
                        routeParts.Add(ReadDeparture(name, lines));
                        break;
                    case "GA_":
                        break;
                    case "GP_":
                        break;
                    case "RWY":
                        break;
                    case "TR_":
                        break;
                    default:
                        throw new Exception("Unexpected route part name: '" + name + "' in file " + fileName);
                }
            }

            return routeParts;
        }

        private static Departure ReadDeparture(string name, IReadOnlyList<string> lines)
        {
            Departure departure = new Departure(name);

            int i = 0;
            while(true)
            {
                if(i >= lines.Count) throw new Exception("No points found for departure " + name);
                if(lines[i++].Trim() == "[POINT]") break;
            }

            List<Point> points = new List<Point>();

            while(true)
            {
                if(i >= lines.Count) break;
                string line = lines[i++];
                if(string.IsNullOrWhiteSpace(line)) break;

                string[] values = line.Split(',');
                if(values.Length != 5) throw new Exception("Expected five values for point in departure " + name);

                Point point = new Point();
                point.East = double.Parse(values[0]);
                point.Altitude = double.Parse(values[1]);
                point.North = double.Parse(values[2]);
                point.Name = values[4];
                points.Add(point);
            }

            departure.Points = points;
            return departure;
        }

        public static IDictionary<string, ISet<string>> ReadAreas(string gameFolder, string airportIcaoCode)
        {
            string routeTablePath = Path.Combine(gameFolder, "PORT", airportIcaoCode, "SCENARIO", "RouteTable.ini");

            IDictionary<string, ISet<string>> areas = new Dictionary<string, ISet<string>>();

            string[] lines = File.ReadAllLines(routeTablePath);
            int index = 0;

            while(index < lines.Length)
            {
                Match areaHeaderMatch = AreaRegex.Match(lines[index++]);
                if(!areaHeaderMatch.Success) continue;

                ISet<string> airports = new HashSet<string>();

                while(index < lines.Length)
                {
                    string line = lines[index++];
                    if(string.IsNullOrWhiteSpace(line)) continue;
                    Match airportMatch = AreaAirportRegex.Match(line);

                    if(!airportMatch.Success)
                    {
                        index--;
                        break;
                    }

                    airports.Add(airportMatch.Groups[1].Captures[0].Value);
                }

                areas.Add(areaHeaderMatch.Groups[1].Captures[0].Value, airports);
            }

            return areas;
        }

        /// <summary>Reads the departure routes from the RouteTable.ini file of the airport.</summary>
        /// <returns>A dictionary from departure route name to the array of route part names making up the route.</returns>
        public static IDictionary<string, string[]> ReadDepartureRoutes(string gameFolder, string airportIcaoCode)
        {
            string routeTablePath = Path.Combine(gameFolder, "PORT", airportIcaoCode, "SCENARIO", "RouteTable.ini");

            IDictionary<string, string[]> departureRoutes = new Dictionary<string, string[]>();

            string[] lines = File.ReadAllLines(routeTablePath);
            int index = 0;

            while(index < lines.Length && !lines[index++].Contains("[DEP_ROUTE]"))
            {
            }

            while(index < lines.Length)
            {
                string line = lines[index++];
                if(line.StartsWith("[")) break;
                if(string.IsNullOrWhiteSpace(line)) continue;

                string[] parts = line.Split(new[] {'='}, StringSplitOptions.RemoveEmptyEntries);
                if(parts.Length != 2) throw new Exception("Incorrect route format found on line " + (index - 1) + ": " + line);

                string name = parts[0].Trim();
                parts = parts[1].Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
                departureRoutes.Add(name, parts.Select(s => s.Trim()).ToArray());
            }

            return departureRoutes;
        }

        /// <summary>Reads the departure and approach route tables from the RouteTable.ini file of the airport.</summary>
        /// <returns>A set containing all route tables from the file.</returns>
        public static ISet<RouteTable> ReadRouteTables(string gameFolder, string airportIcaoCode)
        {
            string routeTablePath = Path.Combine(gameFolder, "PORT", airportIcaoCode, "SCENARIO", "RouteTable.ini");

            ISet<RouteTable> routeTables = new HashSet<RouteTable>();

            string[] lines = File.ReadAllLines(routeTablePath);
            int index = 0;

            while(index < lines.Length)
            {
                string line;
                while(!(line = lines[index++]).Contains("[DEP_TABLE") && !line.Contains("[APP_TABLE")) index++;

                RouteTable routeTable = new RouteTable
                {
                    AreaRoutes = new Dictionary<string, IList<string>>(),
                    TableType = line.Contains("DEP_TABLE") ? RouteTable.RouteTableType.Departure : RouteTable.RouteTableType.Approach
                };

                while(index < lines.Length)
                {
                    line = lines[index++];
                    if(string.IsNullOrWhiteSpace(line)) continue;
                    if(line.StartsWith("["))
                    {
                        index--;
                        break;
                    }

                    string[] parts = line.Split('=');
                    if(parts.Length != 2)
                        throw new Exception("Unexpected line when reading route table, line " + (index - 1) + ": " + line);
                    string name = parts[0].Trim();

                    switch(name)
                    {
                        case "comment":
                            routeTable.Description = parts[1].Trim();
                            break;
                        case "time":
                            string[] times = parts[1].Split('-');
                            if(times.Length != 2)
                                throw new Exception("Unexpected time range value in route table, line " + (index - 1) + ": " + line);
                            routeTable.StartTime = new Time(times[0]);
                            routeTable.EndTime = new Time(times[1]);
                            break;
                        case "wind":
                            string[] winds = parts[1].Split('-');
                            if(winds.Length != 2)
                                throw new Exception("Unexpected wind range value in route table, line " + (index - 1) + ": " + line);
                            routeTable.FromWindDirection = int.Parse(winds[0]);
                            routeTable.ToWindDirection = int.Parse(winds[1]);
                            break;
                        case "visibility":
                            string[] visibilities = parts[1].Split('-');
                            if(visibilities.Length != 2)
                                throw new Exception("Unexpected visibility range value in route table, line " + (index - 1) + ": " + line);
                            routeTable.FromVisibility = int.Parse(visibilities[0]);
                            routeTable.ToVisibility = int.Parse(visibilities[1]);
                            break;
                        case "runway":
                            routeTable.Runways = parts[1].Split(',').Select(s => s.Trim()).ToList();
                            break;
                        case "active":
                            routeTable.ActiveRunways = parts[1].Split(',').Select(s => s.Trim()).ToList();
                            break;
                        default:
                            // Must be an area
                            routeTable.AreaRoutes.Add(name, parts[1].Split('/').Select(s => s.Trim()).ToList());
                            break;
                    }
                }

                routeTables.Add(routeTable);
            }

            return routeTables;
        }
    }
}

﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Atc4Editor.Core.Integration;
using Atc4Editor.Entities;
using Atc4Editor.Entities.Routing;

namespace Atc4Editor.Core
{
    public sealed class ScenarioCreator
    {
        private static readonly Regex AirportDescriptionIataCodeRegex = new Regex(@"\((\w+)\)$");
        private readonly IList<Aircraft> _aircraft;
        private readonly IList<Airline> _airlines;
        private readonly IList<Airport> _airports;
        private readonly IDictionary<string, string> _flightradarAircraftConversion;
        private readonly IList<Model> _models;
        private readonly Random _random;
        private readonly Router _router;
        private readonly IList<Spot> _spots;

        /// <summary>Initializes a new scenario creator. A lot of file reading happens.</summary>
        public ScenarioCreator(string gameFolder, string airportInfoFolder, string airportIcaoCode)
        {
            _spots = InfoReader.ReadSpots(gameFolder, airportInfoFolder, airportIcaoCode);
            _airlines = InfoReader.ReadAirlines(airportInfoFolder);
            _airports = InfoReader.ReadAirports(airportInfoFolder);
            _models = InfoReader.ReadModels(airportInfoFolder);
            _aircraft = InfoReader.ReadAircraft(airportInfoFolder);
            _flightradarAircraftConversion = InfoReader.ReadFlightradarAircraftConversion(airportInfoFolder);
            _router = new Router(gameFolder, airportIcaoCode);
            _random = new Random();
        }

        /// <summary>
        ///     Creates a new scenario file from a schedule file. See the wiki for up to date details on supported file
        ///     formats and options.
        /// </summary>
        /// <param name="outputPath">The path to write the scenario to.</param>
        /// <param name="scheduleCsvPath">The path to the input schedule.</param>
        /// <param name="scenarioStarTime">The start time of the scenario.</param>
        /// <param name="scenarioEndTime">The end time of the scenario.</param>
        public void CreateSchedule(string outputPath, string scheduleCsvPath, Time scenarioStarTime, Time scenarioEndTime)
        {
            DateTime scenarioDateTime = new DateTime(2018, 03, 30, scenarioStarTime.Hour, scenarioStarTime.Minute, 0);
            Time runTime = scenarioEndTime - scenarioStarTime;
            ScenarioWriter writer = new ScenarioWriter(scenarioDateTime, runTime.Hour * 60 + runTime.Minute);
            MovementFactory factory = new MovementFactory();

            string[] lines = File.ReadAllLines(scheduleCsvPath);
            int counter = 1;

            foreach(string line in lines)
            {
                string[] items = line.Split(',');
                InfoReader.AssertCsvCount(4, items.Length, scheduleCsvPath, line);

                DateTime readyDateTime = DateTime.Parse(items[0]);
                Time planeReadyTime = new Time(readyDateTime.Hour, readyDateTime.Minute);
                if(planeReadyTime < scenarioStarTime.AddMinutes(5) || planeReadyTime >= scenarioEndTime)
                    continue; // TODO put these along their route

                string destinationIataCode = AirportDescriptionIataCodeRegex.Match(items[1]).Groups[1].Captures[0].Value;
                string destinationIcaoCode = GetAirportIcaoCode(destinationIataCode);
                string airlineIataCode = items[2].Substring(0, 2);
                string airlineIcaoCode = GetAirlineIcaoCode(airlineIataCode);
                string callSign = airlineIcaoCode + items[2].Substring(2);
                string aircraftCode = items[3];

                Time planeStartTime = scenarioStarTime;

                // Change the time a little to avoid planes piling up
                int timeShift = _random.Next(6) - 2;
                planeReadyTime = planeReadyTime.AddMinutes(timeShift);

                // TODO
                string registration = "JA77" + counter++.ToString().PadLeft(2, '0');
                Model model = GetModel(airlineIcaoCode, aircraftCode);
                Debug.WriteLine($"Chose {model.Folder} for {callSign} {aircraftCode}");
                string spot = GetSpot(model);

                factory.Initialize(model.Folder, model.AircraftIcaoCode, registration, callSign, Movement.MovementType.Departing,
                    spot, true);
                factory.SetDepartureAirport("RJFF");
                factory.SetArrivalAirport(destinationIcaoCode);
                factory.SetStartTime(planeStartTime);
                factory.SetReadyTime(planeReadyTime);

                if(planeReadyTime < scenarioStarTime)
                {
                    factory.SetSection(Section.Gnd);
                    factory.SetState(Movement.MovementState.SnaPushbackStart);
                }

                Route route = _router.GetDepartureRoute(destinationIcaoCode, planeReadyTime);
                factory.SetRoute(route);

                Movement movement = factory.Finish();
                writer.AddMovement(movement);
            }

            writer.WriteToFile(outputPath);
        }

        private string GetSpot(Model model)
        {
            // TODO prefer not too large, take type into account, whether there's a bridge. Also which airlines/destinations hit which terminal.
            List<Spot> rightSize = _spots.Where(s => s.Size >= model.AircraftSize).ToList();
            if(rightSize.Count == 0) throw new Exception("No space for aircraft");
            int i = _random.Next(rightSize.Count);
            Spot spot = rightSize[i];

            RemoveSpot(spot.Name);
            if(spot.ConflictingSpots != null)
                foreach(string conflictingSpotName in spot.ConflictingSpots)
                    RemoveSpot(conflictingSpotName);

            return spot.Name;

            void RemoveSpot(string conflictingSpotName)
            {
                for(int j = _spots.Count - 1; j > 0; j--)
                    if(_spots[j].Name == conflictingSpotName)
                        _spots.RemoveAt(j);
            }
        }

        private Model GetModel(string airlineIcaoCode, string aircraftIcaoCode)
        {
            // Convert the aircraft code to an ICAO one
            if(_flightradarAircraftConversion.ContainsKey(aircraftIcaoCode))
                aircraftIcaoCode = _flightradarAircraftConversion[aircraftIcaoCode];

            // Check whether we have exactly this aircraft
            IList<Model> models =
                _models.Where(m => m.AirlineIcaoCode == airlineIcaoCode && m.AircraftIcaoCode == aircraftIcaoCode).ToList();
            Model model = PickModel();
            if(model != null) return model;

            // Find an alternative aircraft of the same airline
            Aircraft aircraft = _aircraft.FirstOrDefault(a => a.IcaoCode == aircraftIcaoCode)
                                ?? throw new Exception($"Unknown ICAO aircraft type code: {aircraftIcaoCode}");

            foreach(string alternative in aircraft.Alternatives)
            {
                models = _models.Where(m => m.AirlineIcaoCode == airlineIcaoCode && m.AircraftIcaoCode == alternative)
                    .ToList();
                model = PickModel();
                if(model != null) return model;
            }

            // No alternative aircraft of this airline is available. Let go of the airline requirement
            // TODO airline alternatives?
            models =
                _models.Where(m => m.AircraftIcaoCode == aircraftIcaoCode).ToList();
            model = PickModel();
            if(model != null) return model;

            // No aircraft of that type available at all. Try alternatives.
            foreach(string alternative in aircraft.Alternatives)
            {
                models = _models.Where(m => m.AircraftIcaoCode == alternative).ToList();
                model = PickModel();
                if(model != null) return model;
            }

            // Having the worst of luck here. Call in for help.
            throw new Exception($"Cannot decide on mapping for {airlineIcaoCode} {aircraftIcaoCode}");

            Model PickModel()
            {
                if(models.Count == 0) return null;
                if(models.Count == 1) return models[0];

                // TODO deal with unique liveries
                return models[_random.Next(models.Count)];
            }
        }

        private string GetAirlineIcaoCode(string airlineIataCode) =>
            _airlines.FirstOrDefault(a => a.IataCode == airlineIataCode)?.IcaoCode
            ?? throw new Exception($"Unknown airline IATA code: {airlineIataCode}");

        private string GetAirportIcaoCode(string airportIataCode) =>
            _airports.FirstOrDefault(a => a.IataCode == airportIataCode)?.IcaoCode
            ?? throw new Exception($"Unknown airport IATA code: {airportIataCode}");
    }
}

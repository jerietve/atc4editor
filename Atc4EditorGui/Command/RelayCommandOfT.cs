﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Windows.Input;

namespace Atc4EditorGui.Command
{
    public sealed class RelayCommand<T> : ICommand
    {
        private readonly Func<T, bool> _targetCanExecuteMethod;
        private readonly Action<T> _targetExecuteMethod;

        public RelayCommand(Action<T> executeMethod) => _targetExecuteMethod = executeMethod;

        public RelayCommand(Action<T> executeMethod, Func<T, bool> canExecuteMethod)
        {
            _targetExecuteMethod = executeMethod;
            _targetCanExecuteMethod = canExecuteMethod;
        }

        bool ICommand.CanExecute(object parameter)
        {
            if(_targetCanExecuteMethod == null) return _targetExecuteMethod != null;
            T tParameter = (T)parameter;
            return _targetCanExecuteMethod(tParameter);
        }

        // Beware - should use weak references if command instance lifetime is longer than lifetime of UI objects that get hooked up to command
        // Prism commands solve this in their implementation
        public event EventHandler CanExecuteChanged = delegate { };

        void ICommand.Execute(object parameter) => _targetExecuteMethod?.Invoke((T)parameter);

        public void RaiseCanExecuteChanged() => CanExecuteChanged(this, EventArgs.Empty);
    }
}

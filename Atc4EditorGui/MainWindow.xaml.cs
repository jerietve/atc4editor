﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Windows;

namespace Atc4EditorGui
{
    /// <summary>Interaction logic for MainWindow.xaml</summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
    }
}

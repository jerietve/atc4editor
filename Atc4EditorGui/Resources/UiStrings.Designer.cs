﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Atc4EditorGui.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class UiStrings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal UiStrings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Atc4EditorGui.Resources.UiStrings", typeof(UiStrings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Load a scenario using the menu to begin..
        /// </summary>
        public static string LoadScenarioToBegin {
            get {
                return ResourceManager.GetString("LoadScenarioToBegin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to _File.
        /// </summary>
        public static string MenuFile {
            get {
                return ResourceManager.GetString("MenuFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to _Open scenario.
        /// </summary>
        public static string MenuItemOpenScenario {
            get {
                return ResourceManager.GetString("MenuItemOpenScenario", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Airport:.
        /// </summary>
        public static string ScenarioLabelAirport {
            get {
                return ResourceManager.GetString("ScenarioLabelAirport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comments:.
        /// </summary>
        public static string ScenarioLabelComments {
            get {
                return ResourceManager.GetString("ScenarioLabelComments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Creator:.
        /// </summary>
        public static string ScenarioLabelCreator {
            get {
                return ResourceManager.GetString("ScenarioLabelCreator", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date:.
        /// </summary>
        public static string ScenarioLabelDate {
            get {
                return ResourceManager.GetString("ScenarioLabelDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Duration:.
        /// </summary>
        public static string ScenarioLabelDuration {
            get {
                return ResourceManager.GetString("ScenarioLabelDuration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Month:.
        /// </summary>
        public static string ScenarioLabelMonth {
            get {
                return ResourceManager.GetString("ScenarioLabelMonth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saving enabled:.
        /// </summary>
        public static string ScenarioLabelSavingEnabled {
            get {
                return ResourceManager.GetString("ScenarioLabelSavingEnabled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start time:.
        /// </summary>
        public static string ScenarioLabelStartTime {
            get {
                return ResourceManager.GetString("ScenarioLabelStartTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Version:.
        /// </summary>
        public static string ScenarioLabelVersion {
            get {
                return ResourceManager.GetString("ScenarioLabelVersion", resourceCulture);
            }
        }
    }
}

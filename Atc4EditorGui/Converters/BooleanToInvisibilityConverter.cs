﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Atc4EditorGui.Converters
{
    public sealed class BooleanToInvisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(targetType != typeof(Visibility)) throw new InvalidOperationException("The target type must be " + nameof(Visibility));
            if(value is bool invisible) return invisible ? Visibility.Collapsed : Visibility.Visible;
            throw new InvalidOperationException("The input type must be " + nameof(Boolean));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
            value == null || value != (object)Visibility.Visible;
    }
}

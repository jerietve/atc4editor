﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Windows.Controls;

namespace Atc4EditorGui.Scenarios
{
    /// <summary>Interaction logic for ScenarioView.xaml</summary>
    public partial class ScenarioView : UserControl
    {
        public ScenarioView()
        {
            InitializeComponent();
        }
    }
}

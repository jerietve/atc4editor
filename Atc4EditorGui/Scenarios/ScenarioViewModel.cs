﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.ComponentModel;
using System.Windows;
using Atc4Editor.Core;
using Atc4Editor.Entities;
using Atc4EditorGui.Command;
using Microsoft.Win32;

namespace Atc4EditorGui.Scenarios
{
    public sealed class ScenarioViewModel : INotifyPropertyChanged
    {
        private Scenario _scenario;

        public ScenarioViewModel() => MenuItem_OnClickCommand = new RelayCommand(MenuItem_OnClick);

        public bool HasScenario => Scenario != null;

        public Scenario Scenario
        {
            get => _scenario;
            set
            {
                if(_scenario == value) return;
                _scenario = value;
                PropertyChanged(this, new PropertyChangedEventArgs(null));
            }
        }

        public string Comments
        {
            get => Scenario?.Comments == null ? string.Empty : string.Join(Environment.NewLine, Scenario.Comments);
            set => Scenario.Comments = value.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries);
        }

        public RelayCommand MenuItem_OnClickCommand { get; }
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void MenuItem_OnClick()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
                {Filter = @"Decoded scenario files (*.txt)|*.txt;*.jpg|Scenario files (*.sna, *.snax)|*.sna;*.snax"};
            if(openFileDialog.ShowDialog() == true) Scenario = ScenarioReader.ReadScenario(openFileDialog.FileName);
        }
    }
}

﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Windows;

namespace Atc4Explorer
{
    /// <summary>Interaction logic for App.xaml</summary>
    public partial class App : Application
    {
    }
}

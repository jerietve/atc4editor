﻿// This file is licensed to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Atc4Explorer
{
    /// <summary>Interaction logic for MainWindow.xaml</summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Point[] points =
            {
                new Point {X = 35261.76, Y = -30896.78},
                new Point {X = 52811.27, Y = -25750.99},
                new Point {X = 65047.25, Y = -22194.59},
                new Point {X = 157245.16, Y = 55057.53}
            };

            for(int i = 0; i < points.Length - 1; i++)
            {
                Line line = new Line
                {
                    X1 = points[i].X,
                    Y1 = points[i].Y,
                    X2 = points[i + 1].X,
                    Y2 = points[i + 1].Y,
                    Stroke = i % 2 == 0 ? Brushes.Red : Brushes.Blue,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Center,
                    StrokeThickness = 2
                };
                MainCanvas.Children.Add(line);
            }
        }

        private void TransformLines(object sender, EventArgs eventArgs)
        {
            // All children are lines, hopefully
            double minX = double.MaxValue;
            double minY = double.MaxValue;
            double maxX = double.MinValue;
            double maxY = double.MinValue;

            foreach(UIElement child in MainCanvas.Children)
            {
                if(!(child is Line line)) continue;

                double lineMinX = Math.Min(line.X1, line.X2);
                minX = Math.Min(lineMinX, minX);

                double lineMinY = Math.Min(line.Y1, line.Y2);
                minY = Math.Min(lineMinY, minY);

                double lineMaxX = Math.Max(line.X1, line.X2);
                maxX = Math.Max(lineMaxX, maxX);

                double lineMaxY = Math.Max(line.Y1, line.Y2);
                maxY = Math.Max(lineMaxY, maxY);
            }

            double scaleX = MainCanvas.ActualWidth / (maxX - minX);
            double scaleY = MainCanvas.ActualHeight / (maxY - minY);

            foreach(UIElement child in MainCanvas.Children)
            {
                if(!(child is Line line)) continue;
                line.X1 = (line.X1 - minX) * scaleX;
                line.X2 = (line.X2 - minX) * scaleX;
                line.Y1 = (line.Y1 - minY) * scaleY;
                line.Y2 = (line.Y2 - minY) * scaleY;
            }
        }
    }
}
